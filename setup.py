from setuptools import setup

setup(
    name="sms",
    version="0.1.0",
    description="Simple SMS library for django",
    author="Eric Satterwhite",
    author_email="eric@codedependant.net",
    packages=[ "sms" ],
    classifiers=[
        "Framework::Django",
        "Programming Language::Python2.7"
    ]
)
